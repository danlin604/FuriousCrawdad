FuriouosCrawdad's is the hootenany-iest southern home-style bar and restaurant this side of the Mississippi. Come on in for the gumbo and stay a spell for the sweet-tea and sweet-potatoe pie.

Current Openings:

- Host/hostess
- Dish Pig
- Assistant Manager

If any of our jobs appeal to you, feel free to swing on by, drop off yer papers, and say hello to the team. 